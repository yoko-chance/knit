// Package diffusion provides the diffusion of the substances.
package diffusion

import (
	"math"

	"gitlab.com/yoko-chance/textile"
)

// DeltaDistance decide distance that cell on next step be affected cells
var DeltaDistance int = 1

type (
	Option struct {
		// Diffusion coefficient
		Diffusion float64
		// Threshold for active cell
		Threshold int
	}

	Substance struct {
		Amount int
	}

	Line = textile.Line[Substance, Option]
	// Diffusion is implement textile.WeavingLine
	Diffusion struct {
		Line
	}
)

// Evaluate calculates the next state of the diffusion
func (d Diffusion) Evaluate() textile.WeavingLine {
	n := len(d.Cells)
	next := make([]Substance, n)

	for i, cell := range d.Cells {
		next[i].Amount += cell.Amount
		idxs, nears, exists := d.GetNears(i, DeltaDistance)

		for j, near := range nears {
			if i == idxs[j] {
				continue
			}
			if !exists[j] {
				continue
			}

			delta := int(float64(near.Amount) * d.Option.Diffusion / 2 / math.Abs(float64(idxs[j]-i)))

			next[i].Amount += delta
			next[idxs[j]].Amount -= delta
		}
	}

	return Diffusion{Line{
		Cells:   next,
		GetCell: d.GetCell,
		Option:  d.Option,
	}}
}

// String returns the string representation of the diffusion
func (d Diffusion) String() string {
	n := len(d.Cells)
	s := ""

	for i := 0; i < n; i++ {
		s += textile.Stringify[d.Cells[i].Amount > d.Option.Threshold]
	}

	return s
}

// GetAmount returns the amount of the substance in the cell
func (d Diffusion) GetAmount(idx int) int {
	return d.Cells[idx].Amount
}

// SetAmount sets the amount of the substance in the cell
func (d Diffusion) SetAmount(idx int, amount int) {
	d.Cells[idx].Amount = amount
}
