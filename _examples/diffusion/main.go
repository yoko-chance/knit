package main

import (
	"gitlab.com/yoko-chance/knit/diffusion"
	"gitlab.com/yoko-chance/textile"
)

func main() {
	cell_num := 100
	step := 50

	substances := make([]diffusion.Substance, cell_num)
	for i := cell_num * 2 / 5; i < cell_num*3/5; i++ {
		substances[i].Amount = 50
	}

	diffusion.DeltaDistance = 5

	diffusion := diffusion.Diffusion{diffusion.Line{
		Cells:   substances,
		GetCell: textile.GetCellCloseEdge[diffusion.Substance],
		Option: diffusion.Option{
			Diffusion: 0.6,
			Threshold: 5,
		},
	}}

	textile.Weave(step, diffusion)
}
