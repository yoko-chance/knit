// Package reaction provides the reaction of the substances.
package reaction

import "gitlab.com/yoko-chance/textile"

type (
	// Reactants is the interface for the substances that can react
	Reactants interface {
		textile.WeavingLine
		// GetAmount returns the amount of the substance in the cell
		GetAmount(idx int) int
		// SetAmount sets the amount of the substance in the cell
		SetAmount(idx int, amount int)
	}

	Option interface {
		// MakeState is the function to decide the state of the cell
		MakeState(idx int) bool
		// New return new struct of Option
		New() Option
	}

	// Virtual is the virtual substance for the reaction
	Virtual struct {
		state bool
	}

	// Reaction is implement textile.WeavingLine
	Reaction[O Option] struct {
		textile.Line[Virtual, O]
	}
)

// Evaluate calculates the next state of the reaction
func (r Reaction[O]) Evaluate() textile.WeavingLine {
	return Reaction[O]{SetState(textile.Line[Virtual, O]{
		Cells:   r.Cells,
		GetCell: r.GetCell,
		Option:  r.Option.New().(O),
	})}
}

// String returns the string representation of the reaction
func (r Reaction[O]) String() string {
	n := len(r.Cells)
	s := ""

	for i := 0; i < n; i++ {
		s += textile.Stringify[r.Cells[i].state]
	}

	return s
}

// SetState sets the state of the reaction
func SetState[O Option](r textile.Line[Virtual, O]) textile.Line[Virtual, O] {
	n := len(r.Cells)
	l := textile.Line[Virtual, O]{
		Cells:   make([]Virtual, n),
		GetCell: r.GetCell,
		Option:  r.Option,
	}

	for i := 0; i < n; i++ {
		l.Cells[i].state = r.Option.MakeState(i)
	}

	return l
}
